﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Notifications.Base;
using Notifications.Base.MailNotification;
using Notifications.Base.OneSignalNotification;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
      
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private IEventBus _bus;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IEventBus bus)
        {
            _logger = logger;
            _bus = bus;
        }

        [Route("/publish1")]
        [HttpGet]
        public String Getq()
        {
            var message = new MailNotificationMessage("test","test message from rabbitmq11");
            var user = new MailUser("pupalivv@gmail.com");
            var user1 = new MailUser("pupalijj@gmail.com");
            var notification = new MailNotification(new MailUser[]{ user, user1 }, message);
            _bus.Publish(notification);

            var oneSignalMessage = new OneSignalNotificationMessage("test message from rabbitmq11", "some title");
            var oneSignalUser = new OneSignalUser();
            var oneSignalNotification = new OneSignalNotification(oneSignalMessage, new OneSignalUser[] { oneSignalUser });
            _bus.Publish(oneSignalNotification);
            return message.Properties["subject"];
        }

        [Route("/publish")]
        [HttpGet]
        public String Get()
        {
            var message = new MailNotificationMessage("test1", "test message from rabbitmq");
            var user = new MailUser("pupalivv@gmail.com");
            var notification = new MailNotification(new MailUser[] { user }, message);
            _bus.Publish(notification);

            var oneSignalMessage = new OneSignalNotificationMessage("test message from rabbitmq11", "some title");
            var oneSignalUser = new OneSignalUser();
            var oneSignalNotification = new OneSignalNotification(oneSignalMessage, new OneSignalUser[] { oneSignalUser });
            _bus.Publish(oneSignalNotification);
            return message.Properties["subject"];
        }
    }
}
