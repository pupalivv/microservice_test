﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Notifications.Base;

namespace WebApplication1
{
    public class RabbitConfig: IRabbitConfig
    {
        public RabbitConfig(string exName)
        {
            this.ExchangeName = exName;
        }
    }
}
