using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Notifications.Base;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionFactory = new ConnectionFactory() { HostName = "192.168.99.100" };
            var connection = connectionFactory.CreateConnection();
            var rabbitConfig = new RabbitConfig("test");
            services.AddSingleton<IRabbitConfig>(rabbitConfig);
            services.AddSingleton<IConnection>(connection);
            services.AddSingleton<ISubscriptionManger>(new SubscribeManager());
            services.AddSingleton<IEventBus, EventBus>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IConnection con, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();   

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                
            });

            //var channel = con.CreateModel();
            //var queue = channel.QueueDeclare().QueueName;
            //channel.QueueBind(queue, "test", typeof(LogMessage).Name, null);
            //var consumer = new EventingBasicConsumer(channel);
            //consumer.Received += (sender, args) =>
            //{
            //    var file = File.CreateText("test.txt");
            //    file.Write("test message recieved");
            //    file.Close();
            //    logger.LogCritical("recieved the message");
            //};
            //channel.BasicConsume(queue, true, consumer);

        }
    }
}
