﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base
{
    //manages the relation between INotificationMessage and INotificationHandler
    //notificationName : is the NotificationMessage Type Name also the routingKey on rabbitmq exchange
    public interface ISubscriptionManger
    {
        bool IsEmpty { get; }
      
        void AddSubscription<T, TH>()
           where T : INotificationMessage
           where TH : INotificationHandler<T>;

        void RemoveSubscription<T, TH>()
               where T : INotificationMessage
           where TH : INotificationHandler<T>;

        bool HasSubscriptionsForMessage<T>() where T : INotificationMessage;
        Type GetMessageTypeByName(string messageName);
        void Clear();
        Type GetHandlerForMessage<T>() where T : INotificationMessage;
        Type GetHandlerForMessage(string messageName);
        string GetMessageKey<T>();
    }
}
