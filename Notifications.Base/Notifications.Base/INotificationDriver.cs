﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Notifications.Base
{
    public interface INotificationDriver
    {
        public void Send(string message,IUser user, Dictionary<string, string> messageProps);
    }
}
