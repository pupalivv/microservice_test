﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Notifications.Base
{
    public class SubscribeManager : ISubscriptionManger
    {
        public Dictionary<string, Type> subscriptions = new Dictionary<string, Type>();
        public List<Type> messageTypes = new List<Type>();
        public bool IsEmpty => throw new NotImplementedException();

        public void AddSubscription<T, TH>()
            where T : INotificationMessage
            where TH : INotificationHandler<T>
        {
            if (!HasSubscriptionsForMessage<T>())
            {
                Type messageType = typeof(T);
                messageTypes.Add(messageType);
                subscriptions.Add(messageType.Name, typeof(TH));
            }
            return;
        }

        public void Clear()
        {
            messageTypes.Clear();
            subscriptions.Clear();
            return;
        }

        public string GetMessageKey<T>()
        {
            return typeof(T).Name;
        }

        public Type GetMessageTypeByName(string messageName)
        {
            return messageTypes.First((t) => {
                return t.Name == messageName;
            });
        }

        public Type GetHandlerForMessage<T>() where T : INotificationMessage
        {
            var messageName = GetMessageKey<T>();
            return subscriptions[messageName];
        }

        public Type GetHandlerForMessage(string messageName)
        {
            return subscriptions[messageName];
        }

        public bool HasSubscriptionsForMessage<T>() where T : INotificationMessage
        {
            if (messageTypes.Contains(typeof(T)))
            {
                if (subscriptions.ContainsKey(typeof(T).Name))
                {
                    return true;
                }
            }
            return false;
        }

        public void RemoveSubscription<T, TH>()
            where T : INotificationMessage
            where TH : INotificationHandler<T>
        {
            Type messageType = typeof(T);
            if (messageTypes.Contains(messageType))
            {
                messageTypes.Remove(messageType);
                subscriptions.Remove(messageType.Name);
            }
            return;
        }
    }
}
