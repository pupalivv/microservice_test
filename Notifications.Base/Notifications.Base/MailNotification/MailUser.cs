﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base.MailNotification
{
    public class MailUser : IUser
    {
        public string Address { get; set; }
        public Dictionary<string, string> properties { get; set; }

        public MailUser() { }

        public MailUser(string address)
        {
            Address = address;
        }
    }
}
