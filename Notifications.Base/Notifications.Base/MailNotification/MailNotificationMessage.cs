﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base
{
    public class MailNotificationMessage: IMessage
    {
        public MailNotificationMessage() { }

        public MailNotificationMessage(string subject, string templatePath)
        {
            Properties = new Dictionary<string, string>();
            Properties.Add("subject", subject);
            Properties.Add("message", templatePath);
        }

        public Dictionary<string, string> Properties { get; set; }
    }
}
