﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Base.MailNotification
{
    public class MailHandler : INotificationHandler<MailNotification>
    {
        public INotificationDriver GetDriver()
        {
            var driver = new MailDriver("smtp.mailtrap.io", 587, "c6fa088187e2e2", "5b54d0dc5a4aaa", "from@example.com" , true);
            return driver;
        }

        public string GetNotificationMessage(MailNotification message, IUser user)
        {
            return message.Message.Properties["message"];
        }

        public IEnumerable<IUser> GetUsers(MailNotification message)
        {
            return message.Users.ToList();
        }

        public Task Handle(MailNotification message)
        {
            var driver = GetDriver();
            var users = GetUsers(message);
            foreach(var usr in users)
            {
                var userAddress = new MailAddress(usr.Address);
                driver.Send(GetNotificationMessage(message, usr), usr, message.Message.Properties);
            }

            return Task.CompletedTask;
        }
    }
}
