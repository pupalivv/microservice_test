﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base.MailNotification
{
    public class MailNotification : INotificationMessage
    {
        public MailUser[] Users { get; set; }
        public MailNotificationMessage Message { get; set; }

        public MailNotification(){ }

        public MailNotification(MailUser[] user, MailNotificationMessage message)
        {
            Message = message;
            Users = user;
        }
    }
}
