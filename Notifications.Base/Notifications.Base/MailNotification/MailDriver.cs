﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;

namespace Notifications.Base.MailNotification
{
    public class MailDriver : INotificationDriver
    {
        public SmtpClient Client;
        public string From;
        public MailDriver(string host, int port, string account, string password, string from ,bool ssl = false)
        {
            From = from;
            Client = new SmtpClient(host, port)
            {
                EnableSsl = ssl,
                Credentials = new NetworkCredential(account, password),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
        }

        public void Send(string message, IUser user, Dictionary<string, string> messageProps)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add(new MailAddress(user.Address));
            mailMessage.From = new MailAddress(From);
            mailMessage.Subject = messageProps["subject"];
            mailMessage.Body = message;
            Client.Send(mailMessage);
        }
    }
}
