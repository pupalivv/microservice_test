﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Base
{
    public class LogMessageHandler : INotificationHandler<LogMessage>
    {
        public INotificationDriver GetDriver()
        {
            throw new NotImplementedException();
        }

        public string GetNotificationMessage(LogMessage message, IUser user)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IUser> GetUsers(LogMessage message)
        {
            throw new NotImplementedException();
        }

        public Task Handle(LogMessage message)
        {
            Console.WriteLine(message.message);
            return Task.CompletedTask;
        }
    }
}
