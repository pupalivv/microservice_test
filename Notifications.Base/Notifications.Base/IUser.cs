﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base
{
    public interface IUser
    {
        string Address { get; set; }
        Dictionary<string, string> properties { get; set; }
    }
}
