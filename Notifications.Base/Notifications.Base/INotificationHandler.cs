﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Base
{
    //takes a INotificationMessage and handle it with Handle method , Handle method is required to be implemented
    public interface INotificationHandler<T> where T :INotificationMessage
    {
        //gets list of users that should get the notification
        public IEnumerable<IUser> GetUsers(T message);

        //gets the message to be sent to the user in string format
        public string GetNotificationMessage(T message, IUser user);

        //gets the driver that will send the message to the user
        public INotificationDriver GetDriver();


        public Task Handle(T message);
    }
}
