﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Base
{
    public interface IEventBus
    {
        void Publish(INotificationMessage message);

        void Subscribe<T, TH>()
            where T : INotificationMessage
            where TH : INotificationHandler<T>;

        void Unsubscribe<T, TH>()
            where TH : INotificationHandler<T>
            where T : INotificationMessage;
    }
}
