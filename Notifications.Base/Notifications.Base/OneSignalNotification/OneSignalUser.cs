﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base.OneSignalNotification
{
    public class OneSignalUser : IUser
    {
        public OneSignalUser() { }
        public string Address { get; set; }
        public Dictionary<string, string> properties { get; set; }
    }
}
