﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.Json;

namespace Notifications.Base.MailNotification
{
    public class OneSignalDriver : INotificationDriver
    {
        private Guid ApplicationId;
        private string ApiKey;
        private HttpWebRequest Client;

        public OneSignalDriver(string apiKey, Guid appId)
        {
            ApiKey = apiKey;
            ApplicationId = appId;
            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("authorization", "Basic Y2QwZWE4MTktYzc5NS00MDBhLWJlYWEtYjQwYjIwMTc1NzQ5");
            Client = request;
        }

        public void Send(string message, IUser user, Dictionary<string, string> messageProps)
        {
            var param = JsonSerializer.Serialize(new { app_id = ApplicationId, contents = new { en = message }, included_segments = new string[] { "All" } });
            byte[] byteArray = Encoding.UTF8.GetBytes(param);
            using (var writer = Client.GetRequestStream())
            {
                writer.Write(byteArray, 0, byteArray.Length);
            }

            var response = Client.GetResponse();
        }
    }
}
