﻿using Notifications.Base.MailNotification;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Base.OneSignalNotification
{
    public class OneSignalNotificationHandler : INotificationHandler<OneSignalNotification>
    {
        public INotificationDriver GetDriver()
        {
            return new OneSignalDriver("Y2QwZWE4MTktYzc5NS00MDBhLWJlYWEtYjQwYjIwMTc1NzQ5", new Guid("127942be-dda6-4695-bd91-545efb629aac"));
        }

        public string GetNotificationMessage(OneSignalNotification message, IUser user)
        {
            return message.Message.Properties["message"];
        }

        public IEnumerable<IUser> GetUsers(OneSignalNotification message)
        {
            return message.Users;
        }

        public Task Handle(OneSignalNotification message)
        {
            var driver = GetDriver();
            var msg = GetNotificationMessage(message, null);
            driver.Send(msg, null, message.Message.Properties);
            return Task.CompletedTask;
        }
    }
}
