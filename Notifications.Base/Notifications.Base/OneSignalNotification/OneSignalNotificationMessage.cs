﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base.OneSignalNotification
{
    public class OneSignalNotificationMessage : IMessage
    {
        public Dictionary<string, string> Properties { get; set; }
        public OneSignalNotificationMessage() { }
        public OneSignalNotificationMessage(string message, string title)
        {
            Properties = new Dictionary<string, string> { { "title", title }, { "message", message } };
        }
    }
}
