﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base.OneSignalNotification
{
    public class OneSignalNotification : INotificationMessage
    {
        public OneSignalUser[] Users { get; set; }
        public OneSignalNotificationMessage Message { get; set; }

        public OneSignalNotification()
        {

        }

        public OneSignalNotification(OneSignalNotificationMessage message, OneSignalUser[] users)
        {
            Users = users;
            Message = message;
        }
    }
}
