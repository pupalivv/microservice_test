﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Base
{
    interface IMessage
    {
        Dictionary<string, string> Properties { get; set; }
    }
}
