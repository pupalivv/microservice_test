﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.Json;
using System.Threading.Channels;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;

namespace Notifications.Base
{
    public class EventBus : IEventBus
    {
        private IConnection rabbitMqConnection;
        private string exchangeName;
        public ISubscriptionManger subscriptionManger;
        private IModel channel;
        private IServiceProvider provider;
        public EventBus(IConnection connection, ISubscriptionManger subMgr, IRabbitConfig config, IServiceProvider serviceProvider)
        {
            this.subscriptionManger = subMgr;
            this.rabbitMqConnection = connection;
            this.exchangeName = config.ExchangeName;
            provider = serviceProvider;

            CreateChannel();
        }

        public void Publish(INotificationMessage message)
        {
            string jsonMessage = JsonSerializer.Serialize(message, message.GetType());
            Byte[] encodedMessage = Encoding.UTF8.GetBytes(jsonMessage);
            channel.BasicPublish(exchangeName, message.GetType().Name, null, encodedMessage);
        }

        public void Subscribe<T, TH>()
            where T : INotificationMessage
            where TH : INotificationHandler<T>
        {
            if (subscriptionManger.HasSubscriptionsForMessage<T>())
            {
                return;
            }
            subscriptionManger.AddSubscription<T, TH>();
            string queue = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue, exchangeName, typeof(T).Name, null);
            Consume(queue);
        }

        public void Unsubscribe<T, TH>()
            where T : INotificationMessage
            where TH : INotificationHandler<T>
        {
            throw new NotImplementedException();
        }

        private void CreateChannel()
        {
            if (!rabbitMqConnection.IsOpen)
            {
                throw new Exception("Connection to Messagging Server is Closed");
            }
            this.channel = rabbitMqConnection.CreateModel();
            channel.ExchangeDeclare(exchangeName, ExchangeType.Direct);
            return;
        }

        public void Consume(string queueName)
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += BasicConsumer;
            channel.BasicConsume(queueName, false, consumer);
            return;
        }


        private void BasicConsumer(object sender, BasicDeliverEventArgs args)
        {
            string messageName = args.RoutingKey;
            string messageBody = Encoding.UTF8.GetString(args.Body.ToArray());
            ProcessMessage(messageName, messageBody);

        }


        public void ProcessMessage(string messageName, string message)
        {
            Type handlerType = subscriptionManger.GetHandlerForMessage(messageName);
            var handle = handlerType.GetMethod("Handle");
            var handlerInstance = provider.GetService(handlerType);
            var messageInstance = JsonSerializer.Deserialize(message, subscriptionManger.GetMessageTypeByName(messageName));
            handle.Invoke(handlerInstance, new[] { messageInstance });
            return;
        }
    }
}
