﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Notifications.Base;

namespace webapitests
{
    public class RabbitConfig: IRabbitConfig
    {
        public RabbitConfig(string exchange)
        {
            ExchangeName = exchange;
        }
    }
}
