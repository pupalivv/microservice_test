using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using Notifications.Base;
using Microsoft.VisualBasic;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Notifications.Base.MailNotification;
using System.Net.Mail;
using Notifications.Base.OneSignalNotification;

namespace webapitests
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public ILogger logger { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var rabbitFactory = new ConnectionFactory() { HostName = "192.168.99.100" };
            var connection = rabbitFactory.CreateConnection();
            RabbitConfig config = new RabbitConfig("test");
            services.AddSingleton<IRabbitConfig>(config);
            services.AddSingleton<IConnection>(connection);
            services.AddSingleton<ISubscriptionManger>(new SubscribeManager());
            services.TryAddSingleton<IEventBus, EventBus>();
            //services.AddSingleton(typeof(LogMessageHandler1));
            services.AddSingleton(typeof(MailHandler));
            services.AddSingleton(typeof(OneSignalNotificationHandler));
            
            services.AddControllers();
     
        
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> log, IEventBus bus)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            StartListening(log, bus);
        }

        public void StartListening(ILogger logger, IEventBus bus)
        {
            //bus.Subscribe<LogMessage, LogMessageHandler1>();
            bus.Subscribe<MailNotification, MailHandler>();
            bus.Subscribe<OneSignalNotification, OneSignalNotificationHandler>();
        }
    }
}
